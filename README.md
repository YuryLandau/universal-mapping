# Universal mapping

Repository created exclusively to test the most diverse mapping tools.

### `yarn start` / `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test` / `npm test`

Launches the test runner in the interactive watch mode.

## Sobre o documento

# Referências
- Latlong.net: https://www.latlong.net