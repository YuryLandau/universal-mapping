import { MapContainer, TileLayer, CircleMarker, Popup, AttributionControl, Tooltip  } from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'

import teslaData from '../Data/tesla-Chage-Sites.json'

const lat = -19.918180;
const long = -43.937050;

function Leaflet(){

    return(

        <MapContainer 
            center={[lat, long]} 
            zoom={13} 
            scrollWheelZoom={true}
            attributionControl={false}
            >
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />

            <AttributionControl position="bottomright" prefix={false} />
            
            <MarkerClusterGroup
            spiderfyDistanceMultiplier={20}
            showCoverageOnHover={false}
            >
                {teslaData.map((point, key) => {

                    const street = point.address.street;
                    const latitude = point.gps.latitude;
                    const longitude = point.gps.longitude

                    return (
                        <CircleMarker
                            // [lat,long]
                            key={point.id}
                            center={[latitude, longitude]}
                            position={[point.gps.latitude, point.gps.longitude]}
                            >

                            {/* <Popup>
                                {street}
                            </Popup> */}

                            <Tooltip direction='center' offset={[0, -20]} opacity={1}>
                                <span>{street}</span>
                                {/* <span>{loc}</span> */}
                            </Tooltip>
                        </CircleMarker>
                    )
                })}
            </MarkerClusterGroup>

        </MapContainer>
    )
}

export { Leaflet }