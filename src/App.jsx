import { Leaflet } from './Leaflet/LeafletMap'
import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Leaflet />
      </header>
    </div>
  );
}

export default App;
