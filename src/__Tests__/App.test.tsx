import { render, screen } from '@testing-library/react';
import App from '../App';

describe('Testando o componente App', () => {
  render(<App />);

  test("O Link carregou?", () => {

    const linkElement = screen.getByText(/learn react/i);
    expect(linkElement).toBeInTheDocument();
  })

});
